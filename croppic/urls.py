from django.conf.urls import patterns, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from croppic import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('app.views',
    url(r'^$', 'home', name='home'),
    url(r'^upload_pic/$', 'upload_pic', name='upload_pic'),
    url(r'^crop_pic/$', 'crop_pic', name='crop_pic'),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
