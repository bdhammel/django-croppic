from django.core.files.storage import default_storage
from django.shortcuts import render
from django.http import HttpResponse 

import json
from PIL import Image

from app.models import Picture
from app.forms import PicForm, CropForm

def home(request):
    """Display all of the uploaded images
    """
    return render(request, 'index.html', {'pics':Picture.objects.all()})

def upload_pic(request):
    """Handel the initial upload of the image

    Return:
        Successful Upload:
            status (str): "success"
            url (str): the url of the image
            width (float): The width in pixels of the image
            Height (float): the height of the image in pixels
    """

    response_data = {"status":"error", 'message':'Only Post Accepted'}

    if request.method == 'POST':
        form = PicForm(request.POST, {'image':request.FILES['img']})
        if form.is_valid():
            pic = form.save()
            original = Image.open(pic.image)
            width, height = original.size # needed for croppic's zoom feature
            response_data = {
                            "status":"success", 
                            "url":pic.image.url,
                            "width":width,
                            "height":height
                            }
        else:
            response_data = {"status":"error", 'message':form.errors}

    # Croppic will parse the information returned into json. content_type needs 
    # to be set as 'text/plain'
    return HttpResponse(json.dumps(response_data), 
                        content_type="text/plain")

def crop_pic(request):
    """Handel the cropping of the Image

    POST
    ----
    data:
        imgUrl = forms.CharField(max_length=1000)   # your image path (the one we received after successful upload)
        imgInitW = forms.DecimalField() 	    # your image original width (the one we received after upload)
        imgInitH = forms.DecimalField()	            # your image original height (the one we received after upload)
        imgW = forms.DecimalField()		    # your new scaled image width
        imgH = forms.DecimalField()		    # your new scaled image height
        imgX1 = forms.DecimalField()		    # top left corner of the cropped image in relation to scaled image
        imgY1 = forms.DecimalField()		    # top left corner of the cropped image in relation to scaled image
        cropW = forms.DecimalField()		    # cropped image width
        cropH = forms.DecimalField()		    # cropped image height

    Returns:
        Successful upload:
            status (str): "success"
            url (str): the full url of the image
        Unsuccessful upload:
            status (str): "error"
            message (list-str): a list of the errors from the django model form
    """

    response_data = {"status":"error", 'message':'Only Post Accepted'}

    if request.method == 'POST':
        form = CropForm(request.POST)
        if form.is_valid():

            # get the url of the working image i.e. www.example.com/media/pictures/uploaded_image.png
            image_url = form.cleaned_data['imgUrl'] 

            # get the root url for media files i.e. www.example.com/media/      
            media_url = default_storage.base_url 

            # strip the root url off the image url to get it's path i.e. pictures/uploaded_image.png 
            # The purpose of splitting the url at '?' is in the case a querystring is attached to the URL
            image_path = image_url.replace(media_url, '').split('?')[0] 

            pic = Picture.objects.get(image=image_path)
            original = Image.open(pic.image)

            newim = original.resize(
                        (form.cleaned_data['imgW'], form.cleaned_data['imgH']), 
                        Image.ANTIALIAS
                        )

            x1 = form.cleaned_data['imgX1'] 
            y1 = form.cleaned_data['imgY1'] 
            x2 = form.cleaned_data['cropW'] + x1 
            y2 = form.cleaned_data['cropH'] + y1 
            newim = newim.crop((x1,y1,x2,y2))

            #Open the uploaded image and save the cropped image in it's location 
            old_image = default_storage.open(image_path, "w")
            newim.save(old_image, newim.format)
            old_image.close()

            response_data = {
                "status":"success",
                "url":default_storage.url(pic.image),
                }
        else:
            response_data = {"status":"error", 'message':form.errors}

    # Croppic will parse the information returned into json. content_type needs 
    # to be set as 'text/plain'
    return HttpResponse(json.dumps(response_data), 
                        content_type="text/plain")

